const createBttn = (textContent, classList) => {
  const bttn = document.createElement("button");

  bttn.textContent = textContent;
  bttn.classList = [...classList, "filterButtons"];

  return bttn;
};

const createNewElement = (devsList) => {
  let filtersContainer = document.getElementById("filtersContainer");

  if (!filtersContainer) {
    filtersContainer = document.createElement("div");
    filtersContainer.id = "filtersContainer";
  } else if (filtersContainer) {
    filtersContainer.textContent = "";
  }

  for (let i = 0; i < devsList.length; i++) {
    const devsObj = devsList[i];
    const filtro = document.createElement("div");

    Object.keys(devsObj).forEach((key) => {
      const bttnContainer = document.createElement("div");
      bttnContainer.id = "bttnContainer";

      const filterBtn = createBttn("filtrar", [key, "filterBtn"]);
      const copyBtn = createBttn("copiar", [key, "copyBtn"]);
      const delFilterBtn = createBttn("deletar", [key, "delFilterBtn"]);

      const p = document.createElement("p");
      p.textContent = key;

      bttnContainer.appendChild(filterBtn);
      bttnContainer.appendChild(copyBtn);
      bttnContainer.appendChild(delFilterBtn);

      filtro.appendChild(p);
      filtro.appendChild(bttnContainer);
      filtersContainer.appendChild(filtro);
    });
  }

  document.body.appendChild(filtersContainer);
};

chrome.storage.onChanged.addListener((changes, _) => {
  if (Object.keys(changes).includes("devs")) {
    chrome.storage.sync.get("devs", ({ devs }) => {
      if (devs) {
        createNewElement(devs);
      }
    });
  }
});

const localStorage = chrome.storage.sync.get("devs", ({ devs }) => {
  if (devs) {
    createNewElement(devs);
  }
});

document.addEventListener(
  "DOMContentLoaded",
  () => {
    let checkPageButton = document.getElementById("btn");
    checkPageButton.addEventListener(
      "click",
      () => {
        let devsNames = document.getElementById("devsNames");
        let setName = document.getElementById("setName");
        chrome.storage.sync.get("devs", ({ devs }) => {
          const devsList = typeof devs === "object" ? devs : [];

          if (!devsNames.value.trim() | setName.value) {
            return "";
          }

          const objectSave = {};
          objectSave[setName.value] = devsNames.value.trim();
          devsList.push(objectSave);

          chrome.storage.sync.set({ devs: devsList });

          devsNames.value = "";
          setName.value = "";
        });
      },
      false
    );
  },
  false
);

document.addEventListener(
  "DOMContentLoaded",
  () => {
    document.addEventListener(
      "click",
      (event) => {
        const clsList = event.target.className.split(",");

        const setName = clsList[0];
        const delOrFil = clsList[1];

        chrome.storage.sync.get(null, (storageItems) => {
          const devsList = storageItems.devs;
          const filter = storageItems.filter || false;

          const selectedSet = devsList?.filter((obj) => {
            return Object.keys(obj).includes(setName);
          })[0];

          switch (delOrFil) {
            case "delFilterBtn":
              chrome.storage.sync.get("devs", ({ devs }) => {
                devs.map((obj) => {
                  if (Object.keys(obj).includes(setName)) {
                    const filterIndex = devs.findIndex((dev) => obj === dev);
                    devs.splice(filterIndex, 1);
                    chrome.storage.sync.set({ devs: devs });
                  }
                });
              });
              break;

            case "copyBtn":
              chrome.storage.sync.get("devs", ({ devs }) => {
                devs.forEach((obj) => {
                  let [objKey, objValue] = Object.entries(obj)[0];

                  if (objKey.includes(setName)) {
                    navigator.clipboard
                      .writeText(objValue)
                      .then(() => console.log(`Filter copiado: ${objValue}`))
                      .catch((err) =>
                        console.error(`Erro ao copiar o texto: ${err}`)
                      );
                  }
                });
              });

            default:
              if (selectedSet && !filter.bool) {
                chrome.storage.sync.set({
                  filter: {
                    set: Object.values(selectedSet)[0].split(", "),
                    bool: true,
                  },
                });
              } else if (selectedSet && filter.bool) {
                chrome.storage.sync.set({
                  filter: {
                    set: Object.values(selectedSet)[0].split(", "),
                    bool: false,
                  },
                });
              }
              break;
          }
        });
      },
      false
    );
  },
  false
);
